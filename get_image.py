#! -*- coding: utf-8 -*-

import os
import six.moves.cPickle as pickle
import numpy as np
import cv2 as cv
import Parameter
def get_image_gray(image_path):
    image=cv.imread(image_path,0)
    image = cv.resize(image,Parameter.data_size,interpolation=cv.INTER_AREA)
    image=image.reshape(1,Parameter.data_size[0],Parameter.data_size[1]) #2次元 -> 3次元テンソル
    return image

def get_image_color(self,image_path):
    image = cv.imread(image_path,1)
    image = cv.resize(image, Parameter.data_size,interpolation=cv.INTER_AREA)
    image = image.transpose(2,0,1)
    return image

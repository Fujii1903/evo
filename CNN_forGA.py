# -*- coding: utf-8 -*-


import Parameter
import log
from CNN import CNN
from chainer import cuda



class CNN_forGA(CNN):      
    def __init__(self, train_dataset=None, test_dataset=None,data_split=False,test_size=0.2, gpu=-1):
        super(CNN_forGA,self).__init__(train_dataset, test_dataset,data_split,test_size, gpu)
        
    def train_and_test_forGA(self, n_epoch=100, batchsize=100,print_console=True,save_log=False):
        ################################################        
        ## この関数はGAをまわすときにtrain_Alex.pyの中で使用 ##
        ################################################
    
        epoch = 1
        #acu_tmp = 0.0;
        while epoch <= n_epoch:
            #if print_console:print 'epoch', epoch
            ###### train ######
            train_loss,train_accuracy=self.train_one_epoch(batchsize) #0921_This area is "None" byFujino
            #if print_console:print 'train mean loss={}, accuracy={}'.format(train_loss,train_accuracy)
            #if save_log:log.write_train(""+str(epoch)+","+str(train_loss)+","+str(train_accuracy)+"\n")
                 
            ###### evaluation ######
            #test_loss, test_accuracy,y = self.model.forward(self.x_test, self.y_test, train=False, gpu=self.gpu)
            #test_loss= float(cuda.to_cpu(test_loss.data))
            #test_accuracy = float(cuda.to_cpu(test_accuracy.data))
	    
            #if print_console:print 'test mean loss={}, accuracy={}'.format(test_loss, test_accuracy)
            #if save_log:log.write_test(""+str(test_loss)+","+ str(test_accuracy)+"\n")
            """
            ######  escape  ######
            epoch_under=Parameter.epoch_under
            train_threshold=Parameter.train_threshold
            if epoch > epoch_under:
                if train_accuracy <=train_threshold:
                    #print ""+str(epoch)+","+str(train_loss)+","+ str(train_accuracy)+"\n"
                    return None
            """  
            #打ち切りは最大化と最小化で意味が違うので危険　とりやめ
#            if epoch == 1:
#                acu_tmp = train_accuracy
#            
#            epoch_under2=Parameter.epoch_under2
#            train_threshold2=Parameter.train_threshold2
#            if epoch == epoch_under2:
#                if (train_accuracy - acu_tmp) <=train_threshold2:
#                    return None
            ######################
            epoch += 1
            
        test_loss, test_accuracy,y = self.model.forward(self.x_test, self.y_test, train=False, gpu=self.gpu)
        test_loss= float(cuda.to_cpu(test_loss.data))
        test_accuracy = float(cuda.to_cpu(test_accuracy.data))
        #if print_console:print 'test mean loss={}, accuracy={}'.format(test_loss, test_accuracy)
        return train_loss, train_accuracy,test_loss, test_accuracy,y[-1].data #0610戻り値にy[-1].dataを追加..fujino

        
    def test_forCASOOK(self,print_console=True,save_log=True):
        ###### test ######
        loss, accuracy,y = self.model.forward(self.x_test, self.y_test, train=False, gpu=self.gpu)
        loss= float(cuda.to_cpu(loss.data))
        accuracy = float(cuda.to_cpu(accuracy.data))

        if print_console:print 'test mean loss={}, accuracy={}'.format(loss, accuracy)
        if save_log:log.write_test(""+str(loss)+","+ str(accuracy)+"\n")

        return loss,accuracy,y
        

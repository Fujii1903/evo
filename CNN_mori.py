# -*- coding: utf-8 -*-

import time
import six.moves.cPickle as pickle
import numpy as np
from sklearn.datasets import fetch_mldata
#from sklearn.cross_validation import train_test_split
from sklearn.model_selection import train_test_split
from chainer import cuda, Variable, FunctionSet, optimizers, Chain
import chainer.functions as F
import myFunction
from sklearn.cross_validation import StratifiedKFold
import log
import sys
import datetime
import Parameter



class ImageNet(FunctionSet): 
      def __init__(self, n_outputs,sampleData,structures):
          funcs=__import__("chainer.functions",fromlist=["*"]) #funcsから文字列でクラスを呼び出すことができる．
          layers={}
          for layer in structures:
            class_def = getattr(funcs,layer["class"]) #クラス名からクラスを動的に生成
            obj=class_def(**layer["params"]) #指定されたパラメータでインスタンスを作る
            layers[layer["name"]]=obj #名前を指定して層を作る
        
       # 上のループは以下と同じことをしている．structures を変更すれば自動で構造が変わる．任意のクラス名を使用できる．    
       # layers["conv1"] =  F.Convolution2D(Parameter.channel, Parameter.conv1_size, Parameter.filter1_size,pad=Parameter.padding)
       # layers["bn1"]=F.BatchNormalization(Parameter.conv1_size)
            
          super(ImageNet, self).__init__(
            **layers
          )      
        
if __name__ == "__main__":
    structures=[]
    l1={"class":"Convolution2D","name":"conv1","params":{"in_channels":3,"out_channels":10,"ksize":3,"pad":3}}
    l2={"class":"BatchNormalization","name":"bn1","params":{"size":10}}
    structures.append(l1)
    structures.append(l2)
    model=ImageNet(3,[1,2,3],structures)
    print dir(model)
    print dir(model.conv1)
    print model.conv1.out_channels,model.conv1.ksize,model.conv1.pad
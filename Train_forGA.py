# -*- coding: utf-8 -*-

from picture_loader import PictureDataset
from chainer import cuda,optimizers
import sys
import log
import data_function
from CNN_forGA import CNN_forGA
from sklearn.cross_validation import StratifiedKFold#非推奨

#from sklearn.model_selection import StratifiedKFold
import Parameter
from Train import Train

########################
## GAまわすとき用プログラム ##
########################

class Train_forGA(Train):
    
    def __init__(self):
        super(Train_forGA,self).__init__()#あってる？？？
        
        
    def setNetwork_GA(self): 
        dataset = PictureDataset(sys.argv[1],"dataset")
        dataset.load_data_target()
        data=dataset.data
        target=dataset.target
        #print 'n:',n_class
        n_class=dataset.get_n_types_target()      
        #print 'divide dataset'
        kf = StratifiedKFold(target, n_folds=Parameter.k,shuffle=True, random_state=Parameter.seed) 
        #kf = StratifiedKFold(target, n_splits=Parameter.k,shuffle=True, random_state=Parameter.seed)
        #print 'create k-fold data '
        ###### train(test)_data[k][class][num_data][channel][width][height]
        ##### train(test)_target[k][class][num_target]
        train_data=[]
        train_target=[]
        test_data=[]
        test_target=[]
        for index_train,index_test in kf:
            x_train=data[index_train]
            y_train=target[index_train]
            x_test=data[index_test]
            y_test=target[index_test]
                 
            train_data_class,train_target_class=data_function.get_data_split(n_class,x_train,y_train)
            test_data_class,test_target_class=data_function.get_data_split(n_class,x_test,y_test)
            train_data.append(train_data_class)
            train_target.append(train_target_class)
            test_data.append(test_data_class)
            test_target.append(test_target_class)
                
        result=[]
            ###### execution k ######
        exeArray=Parameter.exeArray
                 
        ###### set class combination ######
        train_set=Parameter.train_set
        #print "train set:",train_set
        ###### k-fold loop ######
        k=0
        while k<Parameter.k:
            if not k in exeArray:
                k=k+1
                continue
            #print 'k:'+str(k+1)+'/'+str(Parameter.k)
            #result_f.write('k:'+str(k+1)+'/'+str(Parameter.k)+"\n") #file_write

            x_train,y_train=data_function.get_data(train_data[k],train_set)
            x_test,y_test=data_function.get_data(test_data[k],train_set)

            dir_path="log"
                #if not os.path.exists(dir_path):
                #    os.mkdir(dir_path)
                
            train_dataset=PictureDataset(dir_path,str(k+1)+"dataset")
            train_dataset.data=x_train
            train_dataset.target=y_train
                    
            test_dataset=PictureDataset(dir_path,str(k+1)+"dataset")
            test_dataset.data=x_test
            test_dataset.target=y_test
                    
                    
                    
            cnn=CNN_forGA(train_dataset=train_dataset, test_dataset=test_dataset, gpu=Parameter.gpu)
            
            #cnn.dump_model("log/"+str(k+1)+"_model_before.pkl")
            #log.write_Parameter(k+1)
            
            #tmp_result=cnn.train_and_test(n_epoch=Parameter.epoch,batchsize=Parameter.batchsize)
            tmp_result = cnn.train_and_test_forGA(n_epoch=Parameter.epoch,batchsize=Parameter.batchsize,save_log=True)
#            if tmp_result == None: #Give up if first result (10 epoch) is too bad 
#                result.append(float('nan'))
#                break
            result.append(tmp_result[Parameter.FitResult]) #0929modify
            #print "aaa" + str(tmp_result)
            #result1.append(tmp_result[0])
            #result2.append(tmp_result[2])
            k=k+1 # end of k-fold loop
        print sum(result)
        #print "kaeru"
        #print "k:",k,result2,sum(result2)
        sys.stderr.write("k:"+str(k)+" "+str(result)+"\n")
        return sum(result)


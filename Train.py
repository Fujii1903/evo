# -*- coding: utf-8 -*-
#from CNN import CNN
from picture_loader import PictureDataset
from chainer import cuda,optimizers
import sys
import log
import data_function
from CNN import CNN
import Parameter

class Train(object):
    
    def __init__(self):
        self.name=""
        
    def setSeed(self):    
        Parameter.random.seed(Parameter.seed)
        #print sys.argv

    def useGPU(self):
        if Parameter.gpu==1:
            #print 'use GPU'
            cuda.check_cuda_available()
            #cuda.init(0)
        elif Parameter.gpu==-1:
            print 'use CPU'
        else:
            print 'param.gpu is 1(gpu) or -1(cpu)'
            sys.exit()

    def setParam(self):
        paramGA=sys.argv[2:]#filterSize1,conv1Size,filterSize2,batchSize,Liner1,Liner2,epoch
        Parameter.setParameter(paramGA)
	
    def setNetwork(self): 
        name=str(Parameter.name)
        log.write_param(name)
        #print 'load dataset'
        dataset = PictureDataset(sys.argv[1],"dataset")
        dataset.load_data_target()
        
        data,target,n_class=data_function.getDataset(dataset,Parameter.train_set)
        
        dataset.data=data
        dataset.target=target
        n_class=dataset.get_n_types_target()
        #cnn=CNN(train_dataset=dataset,
        #        gpu=param.gpu)     
        #cnn=CNN(train_dataset=dataset,gpu=param.gpu,paramGA=pamGAra)#add_0705 
        cnn=CNN(train_dataset=dataset,gpu=Parameter.gpu)
  
        #dump start model
        #cnn.dump_model("log/start_"+name+".pkl")

        #print paramGA[6]
        #print paramGA[3]
        loss,acc=cnn.train(n_epoch=Parameter.epoch,batchsize=Parameter.batchsize,save_log=True)
        #print acc
        
        #dump model
        cnn.dump_model("log/"+name+".pkl")


# -*- coding: utf-8 -*-
"""
Created on Tue Jul 05 11:41:36 2016

@author: hasegawa
"""

import os
import sys

argvs = sys.argv
argc = len(argvs)

cp = "./"
debug = False

optimizer = 2# 1: binary sga 2: discrete sga densya

#"common setting(fixed)"
SEED = 1
problemName = "CNNAlexOptimizeProblem"
dname = "exe-jsai-"+problemName
problem = "problem.realworld." + problemName


jar_path = ""

if argc == 1:
    print "-seed : java seed"
    print "-debug : "
    #sys.exit()

elif argc % 2 == 1:
    optionNum = argc / 2 #IvV
    for i in range(optionNum):
        index = i*2 + 1
        option = argvs[index]
        if option == "-seed":
            SEED = int(argvs[index+1])
        elif option == "-debug":
            debug = argvs[index+1]
        else:
            print "¶ÝµÈ¢IvV "+option+" ðgpµÄ¢Ü·"
            sys.exit()
else:
    print "error"
    sys.exit()

##binary
if optimizer != 2:
    clength = 37
    ctype = "population.chrome.BitChrome"
else:##discretized
    clength = 22
    ctype = "population.chrome.DifRangeIntChrome"

########################################## most important ################################ densya
#trainDataPath = "data/newPict"#full path
trainDataPath = "conte/train"#full path full size image
#trainDataPath = "shuffled_Data_5_7/Train"#full path
#trainDataPath = "data_small/newPict"#full path
pythonPath = "python"#full path 
exePath = "train_run_GA.py" # full path
testExePath = "test_run.py" # not make pkl  
#testExePath = "D:/GA_fujinoICPR/train_Alex_exe2.py" # full path
#evalEpochNum = "100"
evalEpochNum = "100"
trainEpochNum = "1"

######################################################################
###                      sga common                                ###
######################################################################
##Control Parameter densya
#cross = "operator.crossover.OnePointCrossover" #or OnePointCrossover
cross = "operator.crossover.UniformCrossover" #or OnePointCrossover
cParam = "1.0" # 0.5~1.0
mParam = str(1.0/clength) #1.0/(clength*2.0) or 2.0/clength
selection = "operator.selection.TournamentSelection" # or RouletteSelection
sParam = "2" #2, 4, 6
#popsize = "24"#40 100#もとは20_fujino
popsize = "6"#40 100#もとは20_fujino

##fixed settings
#cp = "./eclib-mori.jar"
cp = "./eclib-activate.jar"
gabuilder = "gabuilder.basic.DefaultGABuilder"
viewer = "viewer.rwa.CNNAlexViewer"
printlevel = "1"
gsize = "6"#もとは20_fujino
#gsize = "20"#もとは20_fujino
memory = "true"
elite = "true"
scaling = "problem.function.NegativeFunction"

################### binary + sga ######################
if optimizer == 1:
    mutation = "operator.mutation.BitMutation"

################## discrete + sga ###################
if optimizer == 2:
    mutation = "operator.mutation.DifRangeMutation"

#cp = "../eclib.jar"
#builder = "gabuilder.p3.P3Builder"
"""
if platform.system().find('Windows')>-1:
	jar_path = "\;../commons-math3-3.4.jar"
elif platform.system().find('WOW64')>-1:
	jar_path = "\;../commons-math3-3.4.jar"
elif platform.system().find('Linux')>-1:
	jar_path = ":../commons-math3-3.4.jar"
else:
	print platform.system()
#@«äp
#c = crate[0]
## m = mrate[2]
## t = tsize[0]
## a = asize[1]
## d = dsize[3]
"""

dirName = "result_"+dname
if(not os.path.exists(dirName)):
	os.system("mkdir "+dirName)


filename = "./" + dirName + "/"+ "optimizer" + str(optimizer) + "seed" + str(SEED) + ".txt"
command = "java -Xmx80000m -Xms24000m"
#command = "java -Xmx24000m -Xms12000m"
#command = "java -Xmx16384m -Xms8192m"
command += " -cp " + cp + jar_path
command += " evodl.cnn.CNNGAMain"
command += " -output " + filename
command += " -C " + cross + " -Cparam " + cParam
command += " -M " + mutation + " -Mparam " + mParam
command += " -S " + selection + " -Sparam " + sParam
command += " -P " + problem + " -Pparam " + trainDataPath + ":" + pythonPath + ":" + exePath + ":" + evalEpochNum
command += " -clength " + str(clength)
command += " -seed " + str(SEED)
command += " -gabuilder " + gabuilder
command += " -memory " + memory
command += " -gsize " + gsize
command += " -viewer " + viewer
command += " -printlevel " + printlevel
command += " -memory " + memory
command += " -elitism " + elite
command += " -testexepath " + testExePath
command += " -tEpochNum " + trainEpochNum 
command += " -ctype " + ctype
command += " -popsize " + popsize
command += " -scaling1 " + scaling # uncomment out if problem is minimize function
command += " -debug"

print command
os.system(command )
if debug:
    sys.exit()

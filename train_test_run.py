    # -*- coding: utf-8 -*-
"""
Created on Mon Sep 19 13:25:16 2016

@author: Fujino
"""
from TrainAndTest import TrainAndTest
import sys

"""
if len(sys.argv) != 3:
    print '> python train_run.py [test_data_dir_path] [if you need using with GA,please add "-GA" after testpath]'
    sys.exit()
"""
import chainer
print "chv:",chainer.__version__
def checkHyphen(x):
	if(x[0]=='-'):
		return True
	else:
		return False
option = sys.argv[-1]#最後の要素だけ取り出す

if(not checkHyphen(option)):
    trainTest = TrainAndTest()
    print "setSeed"
    trainTest.setSeed()
    print "use GPUorCPU"
    trainTest.useGPU()
    print "set Parameter"
    trainTest.setParam()
    print "make CNN"
    trainTest.setNetwork()
    

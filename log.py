import Parameter
import sys

train_f=None
test_f=None
param_f=None

def write_train(text):
    train_f.write(text)
    train_f.flush()

def write_test(text):
    test_f.write(text)
    test_f.flush()

def write_param(name):
    global train_f
    if train_f !=None:
        train_f.close()
        test_f.close()
        param_f.close()
    Parameter.set_date()
    global train_f
    train_f=open("log/"+str(name)+"_train.dat","wb")
    global test_f
    test_f=open("log/"+str(name)+"_test.dat","wb")
    global param_f
    param_f=open("log/"+str(name)+"_param.txt","wb")
    
    param_f.write("\n"+str(sys.argv)+"\n\n")
    #param_f.write("data:"+sys.argv[1]+","+sys.argv[2]+"\n")
    param_f.write("###network_information###\n")    
    param_f.write("seed:"+str(Parameter.seed)+"\n")
    #param_f.write("exeArray:"+str(Parameter.exeArray)+"\n")
    param_f.write("train_set:"+str(Parameter.train_set)+"\n")
    param_f.write("optimizer:"+str(Parameter.optimizer)+"\n")
    param_f.write("GPU:"+str(Parameter.gpu)+"\n")
    #param_f.write("epoch:"+str(Parameter.epoch)+"\n")
    param_f.write("CV:"+str(Parameter.k)+"\n")
    #param_f.write("batchsize:"+str(Parameter.batchsize)+"\n")
    param_f.write("data_size:"+str(Parameter.data_size)+"\n")
    param_f.write("channel:"+str(Parameter.channel)+"\n")
    """
##convolutional_layer_size
    param_f.write("###convolutional_layer_size###\n")  
    param_f.write("conv1_size:"+str(Parameter.conv1_size)+"\n")
    param_f.write("conv2_size:"+str(Parameter.conv2_size)+"\n")
    param_f.write("conv3_size:"+str(Parameter.conv3_size)+"\n")
    param_f.write("conv4_size:"+str(Parameter.conv4_size)+"\n")
    param_f.write("conv5_size:"+str(Parameter.conv5_size)+"\n")    
    """    
##convolutional_layer_padding
    param_f.write("###convolutional_layer_padding###\n")  
    param_f.write("conv1_pad:"+str(Parameter.conv1_pad)+"\n")
    param_f.write("conv2_pad:"+str(Parameter.conv2_pad)+"\n")
    param_f.write("conv3_pad:"+str(Parameter.conv3_pad)+"\n")
    param_f.write("padding:"+str(Parameter.padding)+"\n")
    """
##filter_size
    param_f.write("###filter_size###\n")  
    param_f.write("filter1_size:"+str(Parameter.filter1_size)+"\n")
    param_f.write("filter2_size:"+str(Parameter.filter2_size)+"\n")
    param_f.write("filter3_size:"+str(Parameter.filter3_size)+"\n")
    param_f.write("filter4_size:"+str(Parameter.filter4_size)+"\n")
    param_f.write("filter5_size:"+str(Parameter.filter5_size)+"\n")    
    
##pooling
    param_f.write("###pooling_size###\n") 
    param_f.write("pooling1_size:"+str(Parameter.pooling1_size)+"\n")
    param_f.write("pooling2_size:"+str(Parameter.pooling2_size)+"\n")
    param_f.write("pooling3_size:"+str(Parameter.pooling3_size)+"\n")
    """
##stride
    param_f.write("###stride###\n") 
    param_f.write("pooling1_stride:"+str(Parameter.pooling1_stride)+"\n")
    param_f.write("pooling2_stride:"+str(Parameter.pooling2_stride)+"\n")
    """
##linear_network_size
    param_f.write("###linear_network_size###\n") 
    param_f.write("linear_network_size1:"+str(Parameter.linear_network_size1)+"\n")
    param_f.write("linear_network_size2:"+str(Parameter.linear_network_size2)+"\n")
    
##actFunction
    param_f.write("###actFunction###\n") 
    param_f.write("actFunction1:"+str(Parameter.actf1)+"\n")
    param_f.write("actFunction2:"+str(Parameter.actf2)+"\n")
    param_f.write("actFunction3:"+str(Parameter.actf3)+"\n")
    param_f.write("actFunction4:"+str(Parameter.actf4)+"\n")
    param_f.write("actFunction5:"+str(Parameter.actf5)+"\n")
    param_f.write("actFunction6:"+str(Parameter.actf6)+"\n")
    param_f.write("actFunction7:"+str(Parameter.actf7)+"\n")
    param_f.write("actFunction8:"+str(Parameter.actf8)+"\n")
    """
    param_f.flush()    

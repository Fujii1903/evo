import numpy as np
def getDataset(dataset,train_set):
    data=[]
    target=[]

    count_data=0
    for tmp_target in dataset.target:
        flag=False
        count_target=0
        for target_group in train_set:
            for number_target in target_group:
                if tmp_target==number_target:
                    tmp_target=count_target
                    flag=True
                if flag:
                    break
            if flag:
                break
            count_target=count_target+1
        if flag:
            target.append(tmp_target)
            data.append(dataset.data[count_data])

        count_data=count_data+1

    data=np.array(data,np.float32)
    target=np.array(target,np.int32)
    return data,target,len(target)

def get_data_split(n,x,y):
    data=[]
    target=[]
    count_n=0
    while count_n<n:
        data.append([])
        target.append([])
        count_n=count_n+1
    i=0
    for tmp_class in y:
        data[tmp_class].append(x[i])
        target[tmp_class].append(y[i])
        i=i+1
    return data,target
def get_target(index_class,n):
    return [index_class]*n

def get_data(data,same_class_all):
    x=[]
    y=[]
    no_target=0
    for same_class in same_class_all:
        for no_class in same_class:
            x=x+data[no_class]
            y=y+[no_target]*len(data[no_class])
        no_target=no_target+1
    return np.array(x,dtype=np.float32),np.array(y,dtype=np.int32)

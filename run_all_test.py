#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 12:52:16 2017
各話ごとのテストを一括でする
@author: dl-box
"""

import os,sys

pkl = sys.argv[1]
dirName = "data/pict/test/story"
outDirName = "outDir/"

for i in range(6,13):
    num=""
    if i < 10:
        num = "0"+str(i)
    else:
        num = str(i)
    cmd = "python test_run.py "+pkl+" "+dirName+num+" >"+outDirName+"result"+str(i)+".dat"
    print ("cmd:"+cmd)
    os.system(cmd)
    

#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 12:52:16 2017
各話ごとのテストを一括でする
@author: dl-box
"""

import os,sys

train_pklName = "log/2017-2-4-epoch500-gSize12pop16.pkl"
testDirName = "conte/test_story6-12/story"
outDirName = "outDir/2017-2-4_epoch500/"

for i in range(6,13):
    num=""
    if i < 10:
        num = "0"+str(i)
    else:
        num = str(i)
    cmd = "python test_run.py "+train_pklName+" "+testDirName+num+" >"+outDirName+"result_test_conte-"+str(i)+".dat"
    print ("cmd:"+cmd)
    os.system(cmd)
    

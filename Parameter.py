# -*- coding: utf-8 -*-
"""
Created on Thu Jul 07 01:43:45 2016

@author: morinaoki
"""
import sys
import numpy as np
from chainer import optimizers
import datetime

#random seed
random=np.random
seed=0

#実行する k-fold の k の値
exeArray=[0,1,2,3,4,5,6,7,8,9]

#訓練データの分け方
#train_set=[[0,2],[1],[3]]なら
#class1は0番目,2番目のデータセット，
#class2は1番目のデータセット，
#class3は3番目のデータセット
#class_num=36
#class_num=4
#class_num=20
class_num=10
train_set=[[x] for x in range(class_num)]
test_set=[[x] for x in range(class_num)]
#test_set=[[2],[5],[8]]
#test_set = [[0,1,2,3,4,5,6],[7,8,9,10,11,12,13]]#Dir:test_allのとき
#train_set=[[x,x+1] for x in range(37,36+class_num,3)]
#train_set=[[x,x+2] for x in range(36,36+class_num,3)]
#test_set = [[x] for x in range(0,class_num,3)]

###上手く学習できていないときのためのパラメータ###
#最低限学習するepoch
epoch_under=50

#最低限学習するepoch2(GA用)
epoch_under2=10
#訓練識別率の閾値．これを割るともう一度やり直し
#train_threshold=0.55
#train_threshold=0


#訓練識別率の変化の閾値．これを割ると適応度をめっちゃ下げる！
#train_threshold2=0.01
##########################################

#学習方法
#optimizer=optimizers.AdaDelta()
#optimizer=optimizers.AdaDelta(rho=1e-6,eps=1e-9)
#optimizer=optimizers.Adam()
optimizer=optimizers.Adam(alpha=0.0001,beta1=0.99,beta2=0.999)
#optimizer=optimizers.Adam(alpha=0.0001,beta1=0.99,beta2=0.999)
#optimizer=optimizers.AdaDelta(rho=1e-7,eps=1e-11)
#self.optimizer=optimizers.MomentumSGD(lr=0.001,momentum=0.9)
#self.optimizer=optimizers.RMSprop()
#self.optimizer.setup(self.model.collect_parameters())

#GPU=1，CPU=-1
gpu=-1

#学習回数
#epoch = 500
#epoch=4

#epoch=3000
#epoch=1000
#epoch=30
epoch=100


#pickleファイルの名前 日時情報＋targetで自動生成
#mori pickle の名前が紛らわしすぎるので日時情報を自動で付加するようにする
def makePklName(tg):
    today=datetime.date.today()
    dayinfo=str(today.year)+"-"+str(today.month)+"-"+str(today.day)+"-epoch"+str(epoch)
    return dayinfo+"-"+tg

#target="epoch1000"
target="google_mini"
#target="uniform_shuffled_g20pop24"
#target="12class-anime2_3"#24class-1のやつ0,1で訓練/-2のやつ0,2で訓練/-3のやつ1,2で訓練
name = makePklName(target)
#print("name:",name)

#Cross Validationの分割数
k=3

#batch size
#batchsize=200
#batchsize=10
batchsize=15
#batchsize=2

#画像としてデータを見た時の横と縦の長さ
#data_size=(80,80)
data_size=(28,28)
#data_size=(160,160)
#data_size=(50,50)
#data_size=(64,64)
#data_size=(100,100)
#data_size=(78,210)
#data_size=(75,106)
#data_size=(100,141)
#data_size=(52,140)
#data_size = (25,70)
#data_size=(36,98)
#data_size=(26,70)

#データの種類
###2017/10/23   1変更  fujii
channel=1

#ゼロで埋める幅.padding
conv1_pad=(3,3)
conv2_pad=(0,0)
conv3_pad=(0,0)
#Padding*なにか考えます
padding=3

#プーリングをずらす幅
pooling1_stride=(2,2)
pooling2_stride=(2,2)
pooling3_stride=(2,2)

#フィルターの数
conv1_size=32
conv2_size=32
conv3_size=8
conv4_size=8
conv5_size=16

#フィルターの大きさ
filter1_size=(5,5)
filter2_size=(3,3)
filter3_size=(3,3)
filter4_size=(3,3)
filter5_size=(7,7)
#filter1_size=(11,11)
#filter2_size=(11,11)

#プーリングの枠の大きさ
pooling1_size=(7,7)
pooling2_size=(3,3)
pooling3_size=(3,3)


#Convolution2D -> Linear の出力ノード数
linear_network_size1=512
linear_network_size2=32
#linear_network_size1=100
#linear_network_size2=50

#活性化関数
actf1 = 0
actf2 = 0
actf3 = 0   #0929modify
            #print "aaa" + str(tmp_res
actf4 = 1
actf5 = 1
actf6 = 1
actf7 = 0
actf8 = 0 #固定

#GA用FitAccuracy or FitLoss
FitResult = 2 #0:train_loss 1:train_accuracy 2:test_loss 3:test_accuracy

def setParameter(params = []):
    for param in params:
        ps = param.split("::")
        if ps[0] == "F1Num":
            global conv1_size
            conv1_size = int(ps[1])
        if ps[0] == "F1Size":
            global filter1_size
            filter1_size = (int(ps[1]),int(ps[1]))
        if ps[0] == "P1Size":
            global pooling1_size
            pooling1_size = (int(ps[1]),int(ps[1]))
        if ps[0] == "F2Num":
            global conv2_size
            conv2_size = int(ps[1])
        if ps[0] == "F2Size":
            global filter2_size
            filter2_size = (int(ps[1]),int(ps[1]))
        if ps[0] == "P2Size":
            global pooling2_size
            pooling2_size = (int(ps[1]),int(ps[1]))
        if ps[0] == "F3Num":
            global conv3_size
            conv3_size = int(ps[1])
        if ps[0] == "F3Size":
            global filter3_size
            filter3_size = (int(ps[1]),int(ps[1]))
        if ps[0] == "F4Num":
            global conv4_size
            conv4_size = int(ps[1])
        if ps[0] == "F4Size":
            filter4_size = (int(ps[1]),int(ps[1]))
        if ps[0] == "F5Num":
            global conv5_size
            conv5_size = int(ps[1])
        if ps[0] == "F5Size":
            global filter5_size
            filter5_size = (int(ps[1]),int(ps[1]))
        if ps[0] == "L1NodeNum":
            global linear_network_size1
            linear_network_size1 = int(ps[1])
        if ps[0] == "L2NodeNum":
            global linear_network_size2
            linear_network_size2 = int(ps[1])
        if ps[0] == "BSize":
            global batchsize
            batchsize = int(ps[1])
        if ps[0] == "Epoch":
            global epoch
            epoch = int(ps[1])
        if ps[0] == "ActF1":
            global actf1
            actf1 = int(ps[1])
        if ps[0] == "ActF2":
            global actf2
            actf2 = int(ps[1])
        if ps[0] == "ActF3":
            global actf3
            actf3 = int(ps[1])
        if ps[0] == "ActF4":
            global actf4
            actf4 = int(ps[1])
        if ps[0] == "ActF5":
            global actf5
            actf5 = int(ps[1])
        if ps[0] == "ActF6":
            global actf6
            actf6 = int(ps[1])
        if ps[0] == "ActF7":
            global actf7
            actf7 = int(ps[1])
    #return (conv1_size,filter1_size,pooling1_size,conv2_size,pooling2_size,filter3_size,filter4_size,conv5_size,filter5_size,linear_network_size1,linear_network_size2,batchsize,epoch)
            
date=None
model_name=None
from datetime import datetime
def set_date():
    global date
    date=datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    global model_name
    model_name="log/"+date+"_model.pkl"

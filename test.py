import six.moves.cPickle as pickle
import matplotlib.pyplot as plt
import sys
import numpy as np
#from CNN import CNN
from picture_loader import PictureDataset
from chainer import cuda
import data_function
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from CNN import CNN
import Parameter

class Test:

    def __init__(self):
        self.name=""
    
    def useGPU(self):
        if Parameter.gpu==1:
            #print 'use GPU'
            cuda.check_cuda_available()
            #cuda.init(0)
        elif Parameter.gpu==-1:
            print 'use CPU'
        else:
            print 'param.gpu is 1(gpu) or -1(cpu)'
            sys.exit()
    
    def argumentException(self):
        ###### argument exception ######
        if len(sys.argv) < 3:
            print '> python test.py [model_path] [test_data_dir_path]'
            sys.exit()
        elif len(sys.argv) > 3:
            #print '> python test.py [model_path] [test_data_dir_path]'
            Parameter.setParameter(sys.argv[3:])
            #sys.exit()

    def setNetwork(self):
        print 'load dataset'
        dataset=PictureDataset(sys.argv[2],"dataset")
        dataset.load_data_target()
  
        data,target,n_class=data_function.getDataset(dataset,Parameter.test_set)
        dataset.data=data
        dataset.target=target
        n_class=dataset.get_n_types_target()
        print 'target:',target
        print 'set:',Parameter.test_set
        print 'class:',n_class

        print 'load model'
        cnn=CNN(train_dataset=dataset,test_dataset=dataset)
        cnn.load_model(sys.argv[1])

        print 'predict'
        loss,accuracy,y=cnn.test(print_console=True,save_log=False,test_size=50)
        findex=0
        for tmp_y in y[-1].data:
            print tmp_y,"-> class",tmp_y.argmax()  
            #print tmp_y,"-> class",tmp_y.argmax(),Parameter.data_filename[findex]  
            findex += 1
	print ("loss:"+str(loss)+" acc:"+str(accuracy)) 

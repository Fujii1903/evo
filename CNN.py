# -*- coding: utf-8 -*-

import six.moves.cPickle as pickle
import numpy as np
from sklearn.cross_validation import train_test_split
from chainer import cuda, Variable, Chain, optimizers, Chain
import chainer.functions as F
import myFunction

import log

import Parameter



class ImageNet(Chain):
    def __init__(self, n_outputs):
        super(ImageNet, self).__init__(
            ###### create layer ######
            conv1=  F.Convolution2D(None, Parameter.conv1_size, Parameter.filter1_size,pad=Parameter.conv1_pad),
            conv2=  F.Convolution2D(None, Parameter.conv2_size, Parameter.filter2_size,pad=Parameter.conv2_pad),
            l3= F.Linear(None,Parameter.linear_network_size),
            l35=F.Linear(None,Parameter.linear_network_size2),
            l4=     F.Linear(None, n_outputs)
        )
    def __init__(self, n_outputs,sampleData):
        """
            conv1=L.Convolution2D(3,  96, 11, stride=4),
            bn1=L.BatchNormalization(96),
            conv2=L.Convolution2D(96, 256,  5, pad=2),
            bn2=L.BatchNormalization(256),
            conv3=L.Convolution2D(256, 384,  3, pad=1),
            conv4=L.Convolution2D(384, 384,  3, pad=1),
            conv5=L.Convolution2D(384, 256,  3, pad=1),
            fc6=L.Linear(9216, 4096),
            fc7=L.Linear(4096, 4096),x
            fc8=L.Linear(4096, 1000),
        """
        layers={}
        #layers["conv1"] =  F.Convolution2D(Parameter.channel, Parameter.conv1_size, Parameter.filter1_size,pad=Parameter.padding)
        layers["conv1"] =  F.Convolution2D(None, Parameter.conv1_size, Parameter.filter1_size,pad=Parameter.padding)
        layers["bn1"]=F.BatchNormalization(Parameter.conv1_size)
        #y=layers["conv1"](sampleData)
        #y=layers["bn1"](y)
        #y=F.max_pooling_2d(y, ksize=Parameter.pooling1_size, stride=Parameter.pooling1_stride)
        layers["conv2"]=  F.Convolution2D(Parameter.conv1_size, Parameter.conv2_size, Parameter.filter2_size,pad=Parameter.padding)
        layers["bn2"]=F.BatchNormalization(Parameter.conv2_size)
        #y=layers["conv2"](y)
        #y=layers["bn2"](y)
        #y=F.max_pooling_2d(y, ksize=Parameter.pooling2_size, stride=Parameter.pooling2_stride)  
        tmpFilSize=3
        layers["conv3"]=  F.Convolution2D(Parameter.conv2_size, Parameter.conv2_size, tmpFilSize,pad=Parameter.padding)
        #y=layers["conv3"](y)
        layers["conv4"]=  F.Convolution2D(Parameter.conv2_size, Parameter.conv2_size, tmpFilSize,pad=Parameter.padding)
        #y=layers["conv4"](y)
        layers["conv5"]=  F.Convolution2D(Parameter.conv2_size, Parameter.conv2_size, tmpFilSize,pad=Parameter.padding)
        #y=layers["conv5"](y)        
        #y=F.max_pooling_2d(y, ksize=Parameter.pooling3_size, stride=Parameter.pooling3_stride)  
        #nsize = y.data.shape[1]*y.data.shape[2]*y.data.shape[3] 
#        print "nsize",nsize, y.data.shape[1], y.data.shape[2], y.data.shape[3]
        #layers["l3"]=     F.Linear(nsize,Parameter.linear_network_size1)
        layers["l3"]=     F.Linear(None,Parameter.linear_network_size1)
        layers["l35"]=    F.Linear(Parameter.linear_network_size1,Parameter.linear_network_size2)
        layers["l4"]=     F.Linear(Parameter.linear_network_size2, n_outputs)

        self.aFunctions=[F.identity,F.relu, F.leaky_relu,F.elu]
        super(ImageNet, self).__init__(
            **layers
        )
        
    """
    def __init__(self,n_outputs,sampleData,cnn_str):
        layers=makeCNN(sampleData,cnn_str) #cnn_str ネットワーク構造を記載した配列
        super(ImageNet, self).__init__(
            **layers
        ) 
   
    def makeCNN(self,sampleData,cnn_str):
        pass
    """
        ###### forward ######
    def forward(self, x_data, y_data, train=True, gpu=-1, isSVMdata=False,SVMFile=None):
        
        
        if gpu >= 0:
            x_data = cuda.to_gpu(x_data)
            y_data = cuda.to_gpu(y_data)

        x, t = Variable(x_data), Variable(y_data)
        #print "x:",x.data
        #print "t:",t.data
        t_range=len(t.data)

        ###### construct layer ######
        output=[]
        #aFuncNum=self.aFuncNum
	#print "1:",self.aFunctions[Parameter.actf1]
	#print "2:",self.aFunctions[Parameter.actf2]
	#print "3:",self.aFunctions[Parameter.actf3]
	#print "4:",self.aFunctions[Parameter.actf4]
	#print "5:",self.aFunctions[Parameter.actf5]
	#print "6:",self.aFunctions[Parameter.actf6]
	#print "7:",self.aFunctions[Parameter.actf7]
        y=F.max_pooling_2d(self.aFunctions[Parameter.actf1](self.bn1(self.conv1(x))), ksize=Parameter.pooling1_size, stride=Parameter.pooling1_stride)
        output.append(y)
        
        y=F.max_pooling_2d(self.aFunctions[Parameter.actf2](self.bn2(self.conv2(y))), ksize=Parameter.pooling2_size, stride=Parameter.pooling2_stride)
        output.append(y)

        y=self.aFunctions[Parameter.actf3](self.conv3(y))
        y=self.aFunctions[Parameter.actf4](self.conv4(y))
        y=self.aFunctions[Parameter.actf5](self.conv5(y))
        y=F.max_pooling_2d(y, ksize=Parameter.pooling3_size, stride=Parameter.pooling3_stride)
        y=F.dropout(self.aFunctions[Parameter.actf6](self.l3(y)), train=train, ratio=0.5)
        output.append(y)

        y=F.dropout(self.aFunctions[Parameter.actf7](self.l35(y)), train=train,ratio=0.5)
        output.append(y)
        #SVM_training-data_make
        if(isSVMdata):
            SVMFile.write(str(t.data[0])+" "+" ".join(map(str,y.data[0]))+"\n")
            
        y=self.aFunctions[Parameter.actf8](self.l4(y))
        output.append(y)

        # output: loss,accuracy,each_layer_output
        return F.softmax_cross_entropy(y, t), F.accuracy(y,t),output

 #forward2_magic number
    def forward2(self, x_data, y_data, train=True, gpu=-1, isSVMdata=False,SVMFile=None):
        
        
        if gpu >= 0:
            x_data = cuda.to_gpu(x_data)
            y_data = cuda.to_gpu(y_data)

        x, t = Variable(x_data), Variable(y_data)
        #print "x:",x.data
        #print "t:",t.data
        t_range=len(t.data)

        ###### construct layer ######
        output=[]
        #aFuncNum=self.aFuncNum
        y=F.max_pooling_2d(self.aFunctions[Parameter.actf1](self.bn1(self.conv1(x))), ksize=7, stride=Parameter.pooling1_stride)
        output.append(y)
        
        y=F.max_pooling_2d(self.aFunctions[Parameter.actf2](self.bn2(self.conv2(y))), ksize=7, stride=Parameter.pooling2_stride)
        output.append(y)

        y=self.aFunctions[Parameter.actf3](self.conv3(y))
        y=self.aFunctions[Parameter.actf4](self.conv4(y))
        y=self.aFunctions[Parameter.actf5](self.conv5(y))
        y=F.max_pooling_2d(y, ksize=Parameter.pooling3_size, stride=Parameter.pooling3_stride)
        y=F.dropout(self.aFunctions[Parameter.actf6](self.l3(y)), train=train, ratio=0.5)
        output.append(y)

        y=F.dropout(self.aFunctions[Parameter.actf7](self.l35(y)), train=train, ratio=0.5)
        output.append(y)
        #SVM_training-data_make
        if(isSVMdata):
            SVMFile.write(str(t.data[0])+" "+" ".join(map(str,y.data[0]))+"\n")
            
        y=self.aFunctions[Parameter.actf8](self.l4(y))
        output.append(y)

        # output: loss,accuracy,each_layer_output
        return F.softmax_cross_entropy(y, t), F.accuracy(y,t),output


class CNN(object):
   # def __init__(self, train_dataset=None, test_dataset=None,data_split=False,test_size=0.2, gpu=-1):
    def __init__(self, train_dataset=None, test_dataset=None,data_split=False,test_size=0.2, gpu=-1):

        self.n_outputs=0
        if train_dataset!=None:
            self.n_outputs=train_dataset.get_n_types_target()
        elif test_dataset!=None:
            self.n_outputs=test_dataset.get_n_types_target()
     
        self.train_dataset=None
        self.test_dataset=None
        self.x_train=None
        self.y_train=None
        self.x_test=None
        self.y_test=None

        if train_dataset!=None:
            self.setTrainDataset(train_dataset,data_split=data_split)
        if test_dataset!=None:
            self.setTestDataset(test_dataset)
        
        sampleData=Variable(np.array([self.x_train[0],]))
        self.model=None
        if self.n_outputs>0:
            self.model = ImageNet(self.n_outputs,sampleData) #新しいコンストラクタを呼ぶ

        if gpu >= 0:
            self.model.to_gpu()

        self.gpu = gpu

        ###### optimizer ######
        self.optimizer = Parameter.optimizer
        self.optimizer.use_cleargrads() #zero_grad ではなくこちらを使う
        self.optimizer.setup(self.model)
        
    def setTrainDataset(self,dataset,data_split=False):
        if data_split==True:
            self.x_train,\
            self.x_test,\
            self.y_train,\
            self.y_test = train_test_split(train_dataset.data, train_dataset.target, test_size=test_size)
        else:
            self.x_train=dataset.data
            self.y_train=dataset.target
            
        self.n_train = len(self.y_train)

    def setTestDataset(self,dataset):
        self.x_test=dataset.data
        self.y_test=dataset.target
        self.n_test = len(self.y_test)
        

    def train_one_epoch(self,batchsize):
        perm = Parameter.random.permutation(self.n_train)
        sum_train_accuracy = 0
        sum_train_loss = 0

        ###### mini batch train ######
        for i in xrange(0, self.n_train, batchsize):
            x_batch = self.x_train[perm[i:i+batchsize]]
            y_batch = self.y_train[perm[i:i+batchsize]]

            real_batchsize = len(x_batch)

            #self.optimizer.zero_grads()
            self.model.cleargrads()
            #loss, acc,y = self.model.forward(x_batch, y_batch, train=True, gpu=self.gpu)
            #####----------0426改変----------#######
            result=self.model.forward(x_batch, y_batch, train=True, gpu=self.gpu)
            loss=result[0]
            acc=result[1]

          #CNN.py は昔loss,acc,h,yを返していたがそれを最後各層の配列を返すように
        	#変更したため4つの変数が蛙のではなく2つの変数＋最後配列がカエル
        	######fujino##########
            h=result[2][-2]
            y=result[2][-1]

            loss.backward()
            self.optimizer.update()

            sum_train_loss += float(cuda.to_cpu(loss.data)) * real_batchsize
            sum_train_accuracy += float(cuda.to_cpu(acc.data)) * real_batchsize
        return sum_train_loss/self.n_train, sum_train_accuracy/self.n_train #引数0:loss 引数1:精度

        
    def train(self,n_epoch,batchsize=100,print_console=True,save_log=False):
        epoch = 1   
        while epoch <= n_epoch:
            if print_console:print 'epoch', epoch
            loss,accuracy=self.train_one_epoch(batchsize)

            if print_console:print 'train mean loss={}, accuracy={}'.format(loss,accuracy)
            if save_log:log.write_train(""+str(epoch)+","+str(loss)+","+str(accuracy)+"\n")
            epoch += 1

        return loss,accuracy
    

    
    #def test(self,print_console=True,save_log=True):
    #メモリ対策用にtest_size追加 test_sizeずつチェックして最後相加平均を取る
    def test(self,print_console=True,save_log=True,test_size=0):
        if test_size > 0:
            average_accuracy = 0.0
            average_loss = 0.0
            total=len(self.x_test)
            split_num = int(float(total)/test_size)
            last_size = total % test_size
            print total,split_num,last_size
            for i in range(split_num):
                startp=0
                x_sub = self.x_test[startp+i*test_size:startp+(i+1)*test_size]
                y_sub = self.y_test[startp+i*test_size:startp+(i+1)*test_size]
                tmp_len = len(x_sub)
                loss, accuracy,y = self.model.forward(x_sub, y_sub, train=False, gpu=self.gpu)
                loss= float(cuda.to_cpu(loss.data))
                accuracy = float(cuda.to_cpu(accuracy.data))
                average_accuracy += accuracy*tmp_len/total
                average_loss += loss*tmp_len/total
                print "i:",i,i*test_size,(i+1)*test_size
                print str(loss),str(accuracy)
            print "average:",average_accuracy,average_loss
            return average_loss,average_accuracy,y
        ###### test ######
        loss, accuracy,y = self.model.forward(self.x_test, self.y_test, train=False, gpu=self.gpu)
        loss= float(cuda.to_cpu(loss.data))
        accuracy = float(cuda.to_cpu(accuracy.data))

        if print_console:print 'test mean loss={}, accuracy={}'.format(loss, accuracy)
        if save_log:log.write_test(""+str(loss)+","+ str(accuracy)+"\n")

        return loss,accuracy,y
        

    def train_and_test(self, n_epoch=100, batchsize=100,print_console=True,save_log=False):

        epoch = 1
        while epoch <= n_epoch:
            if print_console:print 'epoch', epoch

            ###### train ######
            train_loss,train_accuracy=self.train_one_epoch(batchsize)
            
            if print_console:
                print 'train mean loss={}, accuracy={}'.format(train_loss,train_accuracy)
            if save_log:
                log.write_train(""+str(epoch)+","+str(train_loss)+","+str(train_accuracy)+"\n")
                
            ###### evaluation ######
            test_loss, test_accuracy,y = self.model.forward(self.x_test, self.y_test, train=False, gpu=self.gpu)
            test_loss= float(cuda.to_cpu(test_loss.data))
            test_accuracy = float(cuda.to_cpu(test_accuracy.data))

            if print_console:
                print 'test mean loss={}, accuracy={}'.format(test_loss, test_accuracy)
            if save_log:
                log.write_test(""+str(test_loss)+","+ str(test_accuracy)+"\n")

            ######  escape  ######
            epoch_under=Parameter.epoch_under
            train_threshold=Parameter.train_threshold
            if epoch >epoch_under:
                if train_accuracy <=train_threshold:
                    print ""+str(epoch)+","+str(train_loss)+","+ str(train_accuracy)+"\n"
                    return None
            ######################
            
            epoch += 1
        return (train_loss, train_accuracy,test_loss, test_accuracy )   
    def train_and_test_allEpoch(self, n_epoch=100, batchsize=100,print_console=True,save_log=False):
        epoch = 1
        test_best=0.0;
        test_loss_best=float("inf");
        best_line = "";
        while epoch <= n_epoch:
            ###### train ######
            train_loss,train_accuracy=self.train_one_epoch(batchsize)
            
            ###### evaluation ######
            test_loss, test_accuracy,y = self.model.forward(self.x_test, self.y_test, train=False, gpu=self.gpu)
            test_loss= float(cuda.to_cpu(test_loss.data))
            test_accuracy = float(cuda.to_cpu(test_accuracy.data))
            print "epoch:",epoch, 'train mean loss={}, accuracy={} test mean loss={}, accuracy={}'.format(train_loss, train_accuracy, test_loss, test_accuracy)
            if test_accuracy > test_best or (test_accuracy == test_best and test_loss < test_loss_best):	
                test_best = test_accuracy
                test_loss_best = test_loss
                best_line = "epoch:"+str(epoch)+' train mean loss={}, accuracy={} test mean loss={}, accuracy={}'.format(train_loss, train_accuracy, test_loss, test_accuracy)
                self.dump_model("log/best.pkl")
            epoch += 1
            print "best:",best_line
        #yは各層の出力情報　y[-1]で最終層の情報
        #return (train_loss, train_accuracy,test_loss, test_accuracy,y[-1].data)
        return (train_loss, train_accuracy,test_loss, test_accuracy)

   

    ###### save model ######
    def dump_model(self,name):
        self.model.to_cpu()
        pickle.dump(self.model, open(name, 'wb'), -1)
        if self.gpu>=0:
            self.model.to_gpu()
    ###### load model ######
    def load_model(self,name):
        self.model = pickle.load(open(name,'rb'))
        if self.gpu >= 0:
            self.model.to_gpu()
            
        self.optimizer.setup(self.model)
        
    ##### make SVM #####    
    def make_SVM_DATA(self):##SVM_trainingFILE_make
        file=open("./SVM_log/SVM_train0511.txt","w")
        for i in xrange(0,self.n_train):
            self.model.forward(np.array([self.x_train[i]]), np.array([self.y_train[i]]), train=False, gpu=self.gpu,isSVMdata=True,SVMFile=file)
            #self.optimizer.zero_grads()
            #loss, acc,y = self.model.forward(x_batch, y_batch, train=True, gpu=self.gpu)
            #####----------0426改変----------#######
            #self.model.forward(x_batch, y_batch, train=False, gpu=self.gpu,isSVMdata=True)
        file.close()  
        return 0
        
    def make_SVM_test_DATA(self):
        file=open("./SVM_log/SVM_test_log.txt","w")
        for i in xrange(0,self.n_train):
            self.model.forward(np.array([self.x_train[i]]), np.array([self.y_train[i]]), train=False, gpu=self.gpu,isSVMdata=True,SVMFile=file)
            #self.optimizer.zero_grads()
            #loss, acc,y = self.model.forward(x_batch, y_batch, train=True, gpu=self.gpu)
            #####----------0426改変----------#######
            #self.model.forward(x_batch, y_batch, train=False, gpu=self.gpu,isSVMdata=True)
        file.close()  
        return 0    

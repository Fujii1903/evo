#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 12:52:16 2017
各話ごとのテストを一括でする word版
@author: dl-box
"""

import os,sys

trainDirName = "data/word/trainAll"
testDirName = "data/word/test/story"
outDirName = "outDir/"

for i in range(6,13):
    num=""
    if i < 10:
        num = "0"+str(i)
    else:
        num = str(i)
    cmd = "python train_test_run.py "+trainDirName+" "+testDirName+num+" >"+outDirName+"result_train_test_word"+str(i)+".dat"
    print ("cmd:"+cmd)
    os.system(cmd)
    

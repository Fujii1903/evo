#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 25 12:52:16 2017
各話ごとのテストを一括でする
@author: dl-box
"""

import os,sys

trainDirName = "conte/train"
testDirName = "conte/test_story6-12/story"
outDirName = "outDir/"

for i in range(6,13):
    num=""
    if i < 10:
        num = "0"+str(i)
    else:
        num = str(i)
    cmd = "python train_test_run.py "+trainDirName+" "+testDirName+num+" >"+outDirName+"result_train_test_conte0204-"+str(i)+".dat"
    print ("cmd:"+cmd)
    os.system(cmd)
    

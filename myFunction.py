import chainer.functions as F
import Parameter
import math

def getConvolutionSize(size,f_size):
        #return size-Parameter.filter_size+1
        return size-2*math.floor(float(f_size)/2)

def poolingSize(size,window,stride,f_size):
        return math.ceil((float(getConvolutionSize(size,f_size)-window)/stride)+1)
        #return math.floor(float(size-Parameter.filter_size)/stride+)+1

def func(data_size,pad1_size,f1_size,pooling1_size,pooling1_stride,pad2_size,f2_size,pooling2_size,pooling2_stride):
        return poolingSize(poolingSize(data_size+2*pad1_size,pooling1_size,pooling1_stride,f1_size)+2*pad2_size,pooling2_size,pooling2_stride,f2_size)

def getSize():
        w=func(Parameter.data_size[0],Parameter.conv1_pad[0],Parameter.filter1_size[0],Parameter.pooling1_size[0],Parameter.pooling1_stride[0],Parameter.conv2_pad[0],Parameter.filter2_size[0],Parameter.pooling2_size[0],Parameter.pooling2_stride[0])
        h=func(Parameter.data_size[1],Parameter.conv1_pad[1],Parameter.filter1_size[1],Parameter.pooling1_size[1],Parameter.pooling1_stride[1],Parameter.conv2_pad[1],Parameter.filter2_size[1],Parameter.pooling2_size[1],Parameter.pooling2_stride[1])
        return w*h*Parameter.conv2_size

def convolutionToLinear():
	return F.Linear(getSize(),Parameter.linear_network_size)

#! -*- coding: utf-8 -*-

import os
import six.moves.cPickle as pickle
import numpy as np
import cv2 as cv
import Parameter
#from datetime import datetime   ###2017/10/13 fujii
"""
2017/10/13 fujii
def load_data_target内

imageがなぜが浮動小数点を利用して割り算がなされていない.100/255=0となっている
    100/255.で対処
2*100*141の多重行列にnp.arrayしているからチャネル数がどんどんふくれあがってる
    ここの処理でほしいものは2*100*141なはずなのに4*100*141で勝手に4チャネルになってる
    self.tmpcに画素値を加算して，appendしたら要素をすべて0にすることで対処
"""


class PictureDataset:
    def __init__(self,data_path,name):
        self.data_dir_path = data_path
        if self.data_dir_path[-1] != '/':
            self.data_dir_path =self.data_dir_path+"/"
        self.data = None
        self.target = None
        self.n_types_target = -1
        self.dump_name = self.data_dir_path+name+"_"+str(Parameter.data_size).replace(' ','')+"_"+str(Parameter.channel)# + "_" + str(datetime.now().strftime("%Y_%m_%d_%H_%M_%S"))
        self.image_size = Parameter.data_size                                                                              ###↑2017/10/13 fujii
        self.tmpdata = None #2017_10_13_fujino

    def get_dir_list(self):
        tmp = os.listdir(self.data_dir_path)
        if tmp is None:
            return None
        return sorted([x for x in tmp if os.path.isdir(self.data_dir_path+x)])

    def get_class_id(self, fname):
        dir_list = self.get_dir_list()
        dir_name = filter(lambda x: x in fname, dir_list)
        return dir_list.index(dir_name[0])

    def get_image_gray(self,image_path):
        image=cv.imread(image_path,0)
        image = cv.resize(image,self.image_size,interpolation=cv.INTER_AREA)
        image=image.reshape(1,self.image_size[0],self.image_size[1]) #2次元 -> 3次元テンソル
        return image

    def get_image_color(self,image_path):
        image = cv.imread(image_path,1)
        image = cv.resize(image, self.image_size,interpolation=cv.INTER_AREA)
        image = image.transpose(2,0,1)
        return image




        
    def load_data_target(self):
        if os.path.exists(self.dump_name):
            self.load_dataset()
        if self.target is None:
            dir_list = self.get_dir_list()
            ret = {}
            self.target = []
            target_name = []
            self.data = []


            for dir_name in dir_list:
                file_list = os.listdir(self.data_dir_path+dir_name)
                self.tmpdata = np.empty((2, 100, 141))  # 2017_10_13fujino
                tmpcount = 0#2コマうけとりたい
                for file_name in file_list[:10]:
                    root, ext = os.path.splitext(file_name)
                    if ext == u'.png' or ext == u'.PNG' or ext == u'.jpg' or ext == u'.jpeg':
                        abs_name = self.data_dir_path+dir_name+'/'+file_name
                        # read class id i.e., target
                        class_id = self.get_class_id(abs_name)
                        #print class_id#0612                        
                        self.target.append(class_id)
                        target_name.append(str(dir_name))
                        #print abs_name #0622
                        # read image i.e., data
                        if Parameter.channel == 3:
                            image=self.get_image_color(abs_name)

                        elif Parameter.channel ==1:
                            image=self.get_image_gray(abs_name)

                        #2017_10_13_fujino
                        #2017/10/13 fujii
                        elif Parameter.channel == 0:#4coma_2コマずつでチャネルとしたい！
                            #print "KAERUUUUUUUUUUUUUUU"
                            image = self.get_image_gray(abs_name)
                            image = image/255.

                            #print type(image),image.shape

                            #print "aaa",self.tmpdata.shape
                            #self.tmpdata = np.append(self.tmpdata,image,axis=0)
                            self.tmpdata[tmpcount] += image[0]  #2017/10/13 fujii
                            #print "bbb",self.tmpdata.shape

                            tmpcount+=1 #2コマずつ
                            #print "tmpc:",tmpcount


                        if Parameter.channel == 1 or Parameter.channel == 3:
                            image = image/255.
                            self.data.append(image)
                            #print self.data

                        #2017/10/13 fujii
                        elif tmpcount==2:
                            self.data.append(self.tmpdata)
                            print self.data
                            tmpcount=0
                            self.tmpdata = np.zeros_like(self.tmpdata)


            self.index2name = {}
            for i in xrange(len(self.target)):
                self.index2name[self.target[i]] = target_name[i]
        self.data = np.array(self.data, np.float32)
        self.target = np.array(self.target, np.int32)
        self.dump_dataset()
        print "read comp"


    def get_n_types_target(self):
        if self.target is None:
            self.load_data_target()

        if self.n_types_target is not -1:
            return self.n_types_target

        tmp = {}
        for target in self.target:
            tmp[target] = 0
        return len(tmp)

    def dump_dataset(self):
        pickle.dump((self.data,self.target,self.index2name), open(self.dump_name, 'wb'), -1)

    def load_dataset(self):
        self.data, self.target, self.index2name = pickle.load(open(self.dump_name, 'rb'))

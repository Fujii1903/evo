    # -*- coding: utf-8 -*-
"""
Created on Mon Sep 19 13:25:16 2016

@author: Fujino
"""
from Train import Train
from Train_forGA import Train_forGA
import sys

"""
if len(sys.argv) != 3:
    print '> python train_run.py [test_data_dir_path] [if you need using with GA,please add "-GA" after testpath]'
    sys.exit()
"""
import chainer
#print "chv:",chainer.__version__

train_forGA = Train_forGA()
#print "using Genetic Algorithm"
#0:train_loss 1:train_accuracy 2:test_loss 3:test_accuracy

train_forGA.setSeed()
train_forGA.useGPU()
train_forGA.setParam()
train_forGA.setNetwork_GA()


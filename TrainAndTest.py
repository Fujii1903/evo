# -*- coding: utf-8 -*-
#from CNN import CNN
from picture_loader import PictureDataset
from chainer import cuda
import sys
import log
import data_function
from CNN import CNN
import Parameter

class TrainAndTest(object):
    
    def __init__(self):
        self.name=""
        
    def setSeed(self):    
        Parameter.random.seed(Parameter.seed)
        #print sys.argv

    def useGPU(self):
        if Parameter.gpu==1:
            #print 'use GPU'
            cuda.check_cuda_available()
            #cuda.init(0)
        elif Parameter.gpu==-1:
            print 'use CPU'
        else:
            print 'param.gpu is 1(gpu) or -1(cpu)'
            sys.exit()

    def setParam(self):
        #GAパラメータを外部から指定する場合は3番め以降 訓練ディレクトリ テストディレクトリ GA::1 ... 
        paramGA=sys.argv[3:]#filterSize1,conv1Size,filterSize2,batchSize,Liner1,Liner2,epoch
        Parameter.setParameter(paramGA)

    def setNetwork(self): 
        name=str(Parameter.name)
        log.write_param(name)
        
        #train data set
        train_dataset = PictureDataset(sys.argv[1],"dataset")
        train_dataset.load_data_target()
        
        train_data,train_target,n_class=data_function.getDataset(train_dataset,Parameter.train_set)
        
        train_dataset.data=train_data
        train_dataset.target=train_target
        n_class=train_dataset.get_n_types_target()
        
        #test data set
        test_dataset=PictureDataset(sys.argv[2],"dataset")
        test_dataset.load_data_target()
  
        test_data,test_target,n_class=data_function.getDataset(test_dataset,Parameter.test_set)
        test_dataset.data=test_data
        test_dataset.target=test_target
        
        cnn=CNN(train_dataset=train_dataset,test_dataset=test_dataset,gpu=Parameter.gpu)
  
        #dump start model
        cnn.dump_model("log/start_"+name+".pkl")

        #result=cnn.train_and_test_allEpoch(n_epoch=Parameter.epoch,batchsize=Parameter.batchsize,save_log=True)
        result=cnn.train_and_test_allEpoch(n_epoch=Parameter.epoch,batchsize=Parameter.batchsize,save_log=True)
        print result

        #dump model
        cnn.dump_model("log/"+name+".pkl")

